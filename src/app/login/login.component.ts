import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  public user : object={};
  
  constructor(private auth: AuthenticateService, private router: Router) { }
    /*
    Call here blocker
    Refer http://malsup.com/jquery/block/
    Create service for blocker. Avoid using directly.
    */

    public errorMsg = '';
    public employeeList = [];
    ngOnInit() {
      this.auth.getAllEmployee().subscribe(data => this.employeeList = data);
    }

    authenticate(formLogin: any) {
      let isUser: boolean;
      if (formLogin.username !== '' && formLogin.password !== '') {
        this.employeeList.forEach((emp) => {
          if (emp.username === formLogin.username && emp.password === formLogin.password) {
            this.user = {
              username : formLogin.username,
              userid : emp.id
            }
            window.localStorage.setItem('userDetails', JSON.stringify(this.user));
            isUser = true;
          }
        });
      } else {
        this.errorMsg = 'Please fill both username and password field';
        isUser = false;
        return false;
      }
      isUser === true ? this.router.navigate(['/dashboard']) : this.errorMsg = 'Either username or password is incorrect';
      // Write functionality to reset
    }
}
