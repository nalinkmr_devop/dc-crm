import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashbord',
  templateUrl: './dashbord.component.html',
  styleUrls: ['./dashbord.component.scss']
})

export class DashbordComponent implements OnInit {

  fileContent = '';
  fileType = '';
  currName : string;
  public fileDetails: Array<object>=[];
  filename: any;
  time: string;
  currUserName: string;

  constructor() { }

  // function to upload file.
  public onChange(fileList: FileList) {
    var currUserDetail = localStorage.getItem('userDetails');
    this.currUserName = JSON.parse(currUserDetail).username;

    // extract the name of file
    let fullPath = (<HTMLInputElement>document.getElementById('upload')).value;
    if (fullPath) {
        let startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
        let filename = fullPath.substring(startIndex);
        if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
            this.filename = filename.substring(1);
        }
    }
    
    // get current time

    var today = new Date();
    this.time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();

    // storing file types in an array  
    const file = fileList[0];
    
   
    this.fileType = file.type;
    var get = this.getBase64(file,this.fileDetails,this.filename,this.fileType,this.time);
    
   // this.totalArr.push(file.type);
    console.log(this.fileDetails);
    
  }

  //to convert file to base 64
  getBase64(file : File,fileDetails,filename,fileType,time) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function () {
      fileDetails.push( {
        fileName : filename,
        fileType : fileType,
        fileTime : time,
        fileData : reader.result
      });
      // localStorage.setItem('fileData', JSON.stringify(reader.result));
      localStorage.setItem('fileDetails', JSON.stringify(fileDetails));
    };
    reader.onerror = function (error) {
      console.log('Error: ', error);
    };
  }
 
  // to download the file.
  Download(i){
    alert("download button works");
  }

  ngOnInit() {

  }
}
