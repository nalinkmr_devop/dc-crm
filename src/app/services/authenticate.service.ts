import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IEmployee } from '../employee/employee';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateService {

  public url = '../assets/loginMockData.js';

  constructor(private http: HttpClient) { }

  getAllEmployee(): Observable<IEmployee[]> {
    // make http call
    return this.http.get<IEmployee[]>(this.url);
  }
}
